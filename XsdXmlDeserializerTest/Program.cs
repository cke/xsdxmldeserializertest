﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using XmlGenerator;

namespace XsdXmlDeserializerTest
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				using (var reader = File.OpenText("..\\..\\..\\XmlGenerator\\bin\\debug\\output.xml"))
				{
					XmlSerializer serializer = new XmlSerializer(typeof (RootNode));

					var data = (RootNode) serializer.Deserialize(reader);

					Console.WriteLine("Data:");

					foreach (var parent in data.ParentNode)
					{
						Console.WriteLine("Parent: ");

						foreach (var text in parent.Text)
						{
							Console.Write(text.value);
						}

						Console.WriteLine();
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine("Exception {0}: {1}\r\nStack trace:\r\n{2}\r\n\r\n", ex.GetType().Name, ex.Message, ex.StackTrace);

				if (ex.InnerException != null)
				{
					Console.WriteLine("Inner exception {0}: {1}\r\nStack trace:\r\n{2}\r\n\r\n", ex.InnerException.GetType().Name, ex.InnerException.Message, ex.InnerException.StackTrace);
				}
			}
			Console.ReadKey();
		}
	}
}
