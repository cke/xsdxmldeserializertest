﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XmlGenerator
{
	class Program
	{
		static void Main(string[] args)
		{
			var test = new RootNode();

			test.ParentNode = new ParentNode[2];

			test.ParentNode[0] = new ParentNode { Text = GetTextNodes() };
			test.ParentNode[1] = new ParentNode { Text = GetTextNodes() };

			var overrides = new XmlAttributeOverrides();
			overrides.Add(typeof(ParentNode), new XmlAttributes() { XmlRoot = new XmlRootAttribute() { Namespace = "" } });
			overrides.Add(typeof(TextNode), new XmlAttributes() { XmlRoot = new XmlRootAttribute() { Namespace = "" } });
			overrides.Add(typeof(RootNode), new XmlAttributes() { XmlRoot = new XmlRootAttribute() { Namespace = "" } });

			var serializer = new XmlSerializer(typeof(RootNode), overrides);

			using (var writer = File.CreateText("Output.xml"))
			{
				var ns = new XmlSerializerNamespaces();
				ns.Add(string.Empty, "http://tempuri.org/TestSchema.xsd");
				
				serializer.Serialize(writer, test, ns);
			}

			Console.WriteLine("Xml Written.");
		}

		private static TextNode[] GetTextNodes()
		{
			return new []
				{
					new TextNode { value = "Hello" }, 
					new TextNode { value = "World" } 
				};
		}
	}
}
